import java.io.IOException;
import java.util.List;
import java.util.Stack;

public class Script {
    Stack<String> stack;

    boolean run(String script) throws IOException {
//        System.out.println("脚本是：" + script);
        stack = new Stack<>();

        String[] buffer = script.split(" ");
        for (String word : buffer) {
            if (!exec(word)) return false;
        }
        return true;
    }

    // 目前实现的脚本只含有p2pkh
    boolean exec(String word) throws IOException {
//        System.out.println("入栈运算为：" + word);
        switch (word) {
            case "DUP":
                if (stack.empty()) return false;
                stack.push(stack.peek());
                break;
            case "HASH160":
                if (stack.empty()) return false;
                String s = stack.pop();
                stack.push(Binary.parseHex(s).hash160().toHex());
                break;
            case "EQUALVERIFY":
                if (stack.size() < 2) return false;
                String s1 = stack.pop();
                String s2 = stack.pop();
                System.out.println("栈顶是："+ stack.peek());
                return s1.equals(s2);
            case "CHECKSIG":
                if (stack.size() < 2) return false;
                String pubkey = stack.pop();
                String sig = stack.pop();
                return Binary.checkSig(pubkey, Binary.parseHex(pubkey), Binary.parseHex(sig));
            default:
                stack.push(word);
                break;
        }
//        System.out.println("栈顶是："+ stack.peek());
        return true;
    }

    static String lockScript(String addr) {
        return "DUP HASH160 " + addr + " EQUALVERIFY CHECKSIG";
    }



}
