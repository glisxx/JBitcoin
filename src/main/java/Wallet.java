import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Wallet {
    String address;
    Binary publicKey;
    Binary privateKey;
    List<UTXO> coins;
    long amount;

    Wallet(Binary privateKey) throws IOException {
        this.privateKey = privateKey;
        this.publicKey = privateKey.secp256k1();
        this.address = publicKey.hash160().toHex();
    }

    Wallet() throws IOException {
        byte [] priv = new byte[32];
        for (int i = 0; i < 32; i ++) {
            priv[i] = (byte)(Math.random()*256);
        }

        privateKey = new Binary(priv);
        this.publicKey = privateKey.secp256k1();
        this.address = publicKey.hash160().toHex();
    }

    List<UTXO> findMyMoney(UTXOPool pool) {
        amount = 0;
        List<UTXO> utxos = new ArrayList<UTXO>();
        for (UTXO u : pool.pool.values()) {
            if (u.script.contains(this.address)) {
                utxos.add(u);
                amount += u.value;
            }
        }
        coins = utxos;
        coins.sort(Comparator.comparingLong(o -> o.value));
        return utxos;
    }

    long change;

    List<UTXO> findCoins (long count) {
        List<UTXO> utxos = new ArrayList<>();
        long remain = count;
        for(UTXO u : coins) {
            if (remain > 0) {
                utxos.add(u);
                remain -= u.value;
            } else break;
        }
        change = - remain;
        if (remain < 0) return utxos;
        else return null;
    }


    String unlockUTXO(UTXO utxo) throws IOException {
        String sig = Binary.sign(publicKey.toHex(), privateKey).toHex();
        String pubkey = publicKey.toHex();
        return String.format("%s %s %s",sig,pubkey,utxo.script);
    }

    List<Trade.Input> makeInput (List<UTXO> utxos, int outputID) throws IOException {
        List<Trade.Input> inputs = new ArrayList<>();
        for (UTXO u : utxos) {
            String hash = u.bytes().sha256().toHex();
            Trade.Input input = Trade.makeInput(hash, outputID,  unlockUTXO(u));
            inputs.add(input);
        }
        return inputs;
    }

    Trade pay(long amount, String target, long tip) throws IOException {
        Trade trade = new Trade();
        List<UTXO> utxos = findCoins(amount);
        trade.inputs = makeInput(utxos, 0);
        trade.outputs.add(new UTXO(amount, target));
        trade.outputs.add(new UTXO(change - tip, this.address));
        trade.ready();
        return trade;
    }
}
