import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.ByteBuffer;

import java.security.*;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static void memcpy(int src, ByteBuffer buffer, Integer size) {
        if (size > 4) {
            size = 4;
        }
        for (int i = 0; i < size; i++) {
            byte temp = (byte) ((src >> ((3 - i) * 8)) & 0xff);
            buffer.put(temp);
        }
    }

    public static void memcpy(int src, ByteBuffer byteBuffer) {
        memcpy(src, byteBuffer, 4);
    }

    public static void memcpy(long src, ByteBuffer bb) {
        memcpy((int)src, bb, 4);
    }

    public static String sha256(byte [] bytes) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");

            messageDigest.update(bytes);
            byte byteBuffer[] = messageDigest.digest();
            return Utils.byte2String(byteBuffer);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String sha256(String hex) {
        return sha256(Utils.String2byte(hex));
    }

    // 使用sha 1
    public static String hash160(byte [] bytes) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(bytes);
            byte[] mid = md.digest();
            return Utils.byte2String(mid);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String hash160(String hex) {
        return hash160(Utils.String2byte(hex));
    }

    public static String secp256k1(String  privKey) throws IOException {
        String [] ans = runCmd("python -m secp256k1 privkey -p -k " + privKey);
        if (ans.length > 1) {
            return ans[1].substring(12);
        }
        return "";
    }

    public static boolean checkSig(String message, String pubkey, String sig) throws IOException {
        String ans = runCmd("python -m secp256k1 checksig" +
                " -p " + pubkey +
                " -m " + message +
                " -s " + sig)[0];
        return ans.equals("True");
    }

    public static String sign(String message, String privKey) throws IOException {
        return runCmd("python -m secp256k1 sign -k " + privKey + " -m " + message)[0];
    }

    public static String [] runCmd(String command) throws IOException {
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedReader ebr = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        List<String> ans = new ArrayList<String>();
        String line;
        while ((line = br.readLine()) != null) {
            ans.add(line);
        }
        while ((line = ebr.readLine()) != null) {
            System.err.println(line);
        }
        String [] ret = new String[ans.size()] ;
        ans.toArray(ret);
        return ret;
    }

    public static String byte2String(byte [] bytes) {
        StringBuilder sb = new StringBuilder();
        String stmp;
        for (byte aByte : bytes) {
            stmp = Integer.toHexString(aByte & 0xFF);
            sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
        }
        return sb.toString();
    }

    public static byte[] String2byte(String hex) {
        BigInteger bi = new BigInteger(hex, 16);
        return bi.toByteArray();
    }
}