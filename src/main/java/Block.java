import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


public class Block {

    public class Header {
        long version = 1;
        Binary superHash;
        Binary merkleHash;
        long timeStamp = 0;
        long difficulty = 0;
        long nonce = 0;


        Binary bytes() {
            ByteBuffer bb = ByteBuffer.allocate(80);

            Utils.memcpy((int) this.version, bb, 4);

            bb.put(superHash.bytes);
            bb.put(merkleHash.bytes);

            Utils.memcpy((int) this.timeStamp, bb, 4);
            Utils.memcpy((int) this.difficulty, bb, 4);
            Utils.memcpy((int) this.nonce, bb, 4);

            return new Binary(bb.array());
        }

        Header() {
        }

        Header(Binary superHash, Binary merkleHash, long time, long difficulty, long nonce) {
            this.superHash = superHash;
            this.merkleHash = merkleHash;
            this.timeStamp = time;
            this.difficulty = difficulty;
            this.nonce = nonce;
        }

        Header(JSONObject json) {
            version = json.getInt("version");
            timeStamp = json.getInt("timeStamp");
            difficulty = json.getInt("difficulty");
            nonce = json.getInt("nonce");
            superHash = Binary.parseHex(json.getString("superHash"));
            merkleHash = Binary.parseHex(json.getString("merkleHash"));
        }

        JSONObject toJSON() {
            JSONObject ans = new JSONObject();
            ans.put("version", (int) version);
            ans.put("superHash", superHash.toHex());
            ans.put("merkleHash", merkleHash.toHex());
            ans.put("timeStamp", (int) timeStamp);
            ans.put("difficulty", (int) difficulty);
            ans.put("nonce", (int) nonce);
            return ans;
        }
    }

    long blkSize;
    Header header;
    long tradeCount;
    List<Trade> trades;



    Block(JSONObject json) {
        blkSize = json.getInt("blkSize");

        trades = new ArrayList<>();

        JSONObject jHeader = json.getJSONObject("header");
        header = new Header(jHeader);


        tradeCount = json.getInt("tradeCount");
        JSONArray jTrades = json.getJSONArray("trades");
        for (int i = 0; i < jTrades.length(); i++) {
            trades.add(new Trade(jTrades.getJSONObject(i)));
        }
    }


    JSONObject toJSON() {
        JSONObject ans = new JSONObject();
        ans.put("blkSize", (int) blkSize);
        ans.put("tradeCount", (int) tradeCount);

        ans.put("header", header.toJSON());

        JSONArray trans = new JSONArray();
        for (Trade t : trades) {
            trans.put(t.toJSON());
        }
        ans.put("trades", trans);

        return ans;
    }

    Block() {
    }

    byte [] bytes () {
        ByteBuffer bb = ByteBuffer.allocate((int)blkSize);
        Utils.memcpy((int)blkSize, bb, 4);
        bb.put(header.bytes().bytes);
        Utils.memcpy((int)tradeCount, bb, 4);
        for (Trade t : trades) {
            bb.put(t.bytes());
        }
        return bb.array();
    }

    Block(Binary superHash, MerkleTree tree, long time, long difficulty, long nonce, List<Trade> trades) {
        header = new Header(superHash, tree.root.getHash(), time, difficulty, nonce);
        this.tradeCount = trades.size();
        this.trades = trades;
        this.blkSize = this.size();

    }

    int size() {
        int ans = 0;
        for (Trade t : trades) {
            ans += t.size();
        }
        return ans + 88;
    }

}
