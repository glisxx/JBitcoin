import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    static void log(String s) {
        System.out.println(s);
    }

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        System.out.println("hello world");

        Binary privkey = Binary.parseHex("c6e193266883a500c6e51a117e012d96ad113d5f21f42b28eb648be92a78f92f");
        Binary pubkey = privkey.secp256k1();
        System.out.println("钱包秘钥: " + privkey.toHex());
        Wallet wallet = new Wallet(privkey);
        log("钱包地址：" + wallet.address);


        System.out.println("验证Binary的转换是否有问题");
        System.out.println("bi > " + Binary.parseHex("123465").toHex());

        System.out.println("验证签名");
        Binary sign = Binary.sign(pubkey.toHex(),privkey);
        System.out.println(sign.toHex());
        System.out.println(Binary.checkSig(pubkey.toHex(), pubkey, sign));

        sign = Binary.parseHex("1234567890");
        System.out.println(sign.toHex());
        System.out.println(Binary.checkSig(pubkey.toHex(), pubkey, sign));



        // 以下构筑创世区块
//        log("创世区块的coinbase交易");
//        Coinbase c = new Coinbase((long)250000000, wallet, 250890645);
//
//        List<Trade> trades = new ArrayList<>();
//        trades.add(c);
//
//        log("建立创世区块");
//        MerkleTree merkleTree = new MerkleTree(trades);
//        merkleTree.save();
//        Block block = new Block(merkleTree.root.getHash(), merkleTree, 250890651, (long)0x0000000, (long)1, trades);
//        log(block.toJSON().toString(4));
//        BlockChain.getInstance().addBlockJson(block);
        // 创世区块建立完成

        UTXOPool pool = new UTXOPool();
        Miner miner = new Miner(BlockChain.getInstance(), pool);
        miner.setup();

        pool.printPool();





//        log("测试挖矿");
//        Block.Header header = BlockChain.getInstance().peek().header;
//        long time = new Date().getTime();
//        long difficulty = 4;
//        long nonce = miner.dig(header.superHash, header.merkleHash, (int)time, (int)difficulty);
//        log(nonce + "");
//        Binary hash = new Binary();
//        hash.append(1);
//        hash.append(header.superHash);
//        hash.append(header.merkleHash);
//        hash.append((int)time);
//        hash.append((int)difficulty);
//        hash.append((int)nonce);
//        log(hash.toHex());
//        log(hash.sha256().toHex());



//        Binary binary = Binary.parseHex("0c123823654");
//        log(binary.toHex());



//        miner.makeBlock();




//        System.out.println("新建钱包");
////        Wallet w1 = new Wallet(privkey);
//        Wallet w2 = new Wallet();
//        System.out.println(w2.address);
//        log("新建一笔支付");
//
//        wallet.findMyMoney(pool);
//        Trade t1 = wallet.pay(9, w2.address, 1);
//
//
//        miner.receiveTrade(t1);
//        miner.utxoPool.printPool();
//
//        log("建立新的block并且存入数据");
//        miner.makeBlock(wallet);


    }
}
