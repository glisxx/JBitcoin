import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.util.List;

public class UTXO {
    long value;
    long scriptSize;
    String script;


    Binary bytes() {
        ByteBuffer bb = ByteBuffer.allocate(12 + (int) scriptSize);

        int high = (int) (value >> 32);
        int low = (int) value;

        Utils.memcpy(high, bb, 4);
        Utils.memcpy(low, bb, 4);

        Utils.memcpy((int) scriptSize, bb, 4);
        bb.put(script.getBytes());

        return new Binary(bb.array());
    }

    JSONObject toJSON() {
        JSONObject ans = new JSONObject();
        ans.put("value", (int) value);
        ans.put("scriptSize", (int) scriptSize);
        ans.put("script", script);

        return ans;
    }

    UTXO() {
    }

    UTXO(JSONObject json) {
        value = json.getInt("value");
        scriptSize = json.getInt("scriptSize");
        script = json.getString("script");

    }

    UTXO(long amount, String target) {
        this.value = amount;
        this.script = Script.lockScript(target);
        this.scriptSize = script.getBytes().length;
    }

    int size() {
        return 8 + (int) scriptSize;
    }

}
