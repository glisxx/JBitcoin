import jnr.ffi.annotations.In;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Miner {

    UTXOPool utxoPool;
    BlockChain blockChain;

    List<Trade> tradeCache = new ArrayList<Trade>();

    Miner(BlockChain blockChain, UTXOPool utxoPool) {
        this.blockChain = blockChain;
        this.utxoPool = utxoPool;
    }

    void setup() throws IOException {
        for (int i = 0; i < blockChain.getSize(); i++) {
            Block blk = blockChain.readBlockFile(i, true);
            for (Trade t : blk.trades) {
                if (utxoPool.useTrade(t)) utxoPool.addTrade(t);
            }
        }

//        tradeCache.add(new Coinbase(25, wallet, new Date().getTime()));
    }

    boolean receiveTrade(Trade trade) throws IOException {
        if (checkTrade(trade)) {
            tradeCache.add(trade);
            utxoPool.useTrade(trade);
            utxoPool.addTrade(trade);
            return true;
        }
        return false;
    }

    boolean checkTrade(Trade trade) throws IOException {
        boolean isOK = true;
        for (Trade.Input input : trade.inputs) {
            Script script = new Script();
            if (!(utxoPool.pool.containsKey(input.txid) &&
                    script.run(input.script))) {
                isOK = false;
                System.out.println("验证错误");
            }
        }
        System.out.println("验证成功");
        return isOK;
    }

    void makeBlock(Wallet wallet) throws IOException, NoSuchAlgorithmException {

        tradeCache.add(new Coinbase((long)250000000, wallet, new Date().getTime()));

        MerkleTree merkleTree = new MerkleTree(tradeCache);
        merkleTree.save();
        Binary superHash = blockChain.peek().header.bytes().sha256();
        long time = new Date().getTime();
        long difficulty = 1;
        long nonce = dig(superHash, merkleTree.root.getHash(), (int) time, (int) difficulty);
        Block block = new Block(superHash, merkleTree, time, (long) difficulty, (long) nonce, tradeCache);
        blockChain.addBlockJson(block);
    }

    int dig(Binary superHash, Binary merkleHash, int time, int difficulty) {
        int nonce = -1;
        StringBuilder prefix = new StringBuilder();
        for (int i = 0; i < difficulty; i++) {
            prefix.append("0");
        }
        Binary hash;
        do {
            nonce = nonce + 1;
            hash = new Binary();
            hash.append(1);
            hash.append(superHash);
            hash.append(merkleHash);
            hash.append(time);
            hash.append(difficulty);
            hash.append(nonce);

        } while (!hash.sha256().toHex().startsWith(prefix.toString()));
        return nonce;
    }
}
