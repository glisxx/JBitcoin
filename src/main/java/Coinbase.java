public class Coinbase extends Trade {
    Coinbase (long coin, Wallet wallet, long time) {
        super();
        this.outputCount = 1;
        UTXO utxo = new UTXO();
        utxo.value = coin;
        utxo.script = "DUP HASH160 "+wallet.address+" EQUALVERIFY CHECKSIG";
        utxo.scriptSize = utxo.script.getBytes().length;
        this.outputs.add(utxo);
        this.timeStamp = time;
    }
}
