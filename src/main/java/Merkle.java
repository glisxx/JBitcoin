public class Merkle {
    private Binary hash;
    boolean isLeaf;

    Merkle left = null;
    Merkle right = null;

    public Merkle(Merkle left, Merkle right){
        this.left = left;
        this.right = right;

        this.isLeaf =false;
        this.hash = left.hash.add(right.hash).sha256().sha256();
    }

    public Merkle(Trade trade) {
        this.hash = new Binary(trade.bytes()).sha256().sha256();
        this.isLeaf = true;
    }

    Binary getHash() {
        return hash;
    }




}
