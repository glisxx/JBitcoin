import org.json.JSONObject;
import org.python.antlr.ast.Str;

import java.io.*;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BlockChain {

    List<String> blockChainHash = new ArrayList<String>();


    static BlockChain getInstance () throws IOException {
        if (self == null) self = new BlockChain();
        self.readMenu();
        return self;
    }

    private static BlockChain self;

    private BlockChain() {
    }

    Block readBlockFile(String hash, boolean isJsonFormat) throws IOException {
        Block block = null;
        if (isJsonFormat) {
            File blk = new File("block_chain/json/" + hash);
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(blk)));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            block = new Block(new JSONObject(sb.toString()));
        }
        return block;
    }

    Block readBlockFile(int i, boolean isJsonFormat) throws IOException {
        return readBlockFile(blockChainHash.get(i), isJsonFormat);
    }

    Block peek() throws IOException {
        return readBlockFile(blockChainHash.size() - 1, true);
    }

    synchronized void readMenu() throws IOException {
        File menu = new File("block_chain/menu");
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(menu)));
        String line;
        while ((line = br.readLine()) != null) {
            blockChainHash.add(line);
        }
    }

    synchronized void addMenu(String hash) throws IOException {
        File menu = new File("block_chain/menu");
        FileWriter fw = new FileWriter(menu, true);
        fw.write(hash);
        fw.write("\n");
        fw.flush();
        fw.close();
    }

    void addBlockJson(Block block) throws IOException {
        if (!checkBlock(block)) return;

        String hash = block.header.bytes().sha256().toHex();

        blockChainHash.add(hash);
        addMenu(hash);

        File blk = new File("block_chain/json/"+ hash);
        blk.createNewFile();
        FileWriter fw = new FileWriter(blk);
        fw.write(block.toJSON().toString(4));
        fw.flush();
        fw.close();
    }

    boolean checkBlock(Block block) {
        return true;
    }

    int getSize() {
        return blockChainHash.size();
    }


}
