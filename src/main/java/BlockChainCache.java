import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

interface BlockChainCache {
    void add(Block block) throws IOException;
}

class TwoLayerCache implements BlockChainCache {

    List<Block> singleBlocks = new ArrayList<>();
    Map<String, Block> extendBlocks = new HashMap<>();

    @Override
    public void add(Block block) throws IOException {
        if (block.header.superHash.equals(
                BlockChain.getInstance().peek().header.bytes().sha256())) {
            extendBlocks.put(block.header.bytes().sha256().toHex(), block);
        } else {
            singleBlocks.add(block);
        }

        boolean needClear = false;

        if (singleBlocks.size() > 0 && extendBlocks.size() > 0) {
            for (Block b : singleBlocks) {
                String superKey = b.header.superHash.toHex();
                if (extendBlocks.containsKey(superKey)) {
                    BlockChain.getInstance().addBlockJson(extendBlocks.get(superKey));
                    BlockChain.getInstance().addBlockJson(b);
                    needClear = true;
                    break;
                }
            }
        }

        if (needClear) {
            extendBlocks.clear();
            singleBlocks.clear();
        }

    }
}