import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class UTXOPool {
    Map<String, UTXO> pool = new HashMap<String, UTXO>();


    boolean verify(Trade.Input input) throws IOException {
        Script script = new Script();
        boolean isScript = script.run(input.script);
        if (pool.containsKey(input.txid)) {
            return true;
        } else {
            return false;
        }
    }

    boolean use(Trade.Input input) throws IOException {

        if (verify(input)) {
            pool.remove(input.txid);
            return true;
        } else return false;
    }

    void add(UTXO utxo) {
        String hash = utxo.bytes().sha256().toHex();
        pool.put(hash, utxo);
    }

    void addTrade(Trade t) {
        for (UTXO u : t.outputs) {
            add(u);
        }
    }

    boolean useTrade(Trade t) throws IOException {
        boolean ans = true;
        for (Trade.Input i : t.inputs) {
            ans = ans && use(i);
        }
        return ans;
    }

    void printPool(){
        for (String key : pool.keySet()) {
            System.out.println(key);
            System.out.println(pool.get(key).toJSON().toString(4));
        }
    }
}
