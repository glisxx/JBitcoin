import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class MerkleTree {

    Merkle root;

    Merkle[] merkles;

    MerkleTree(List<Trade> trades){
        int size = trades.size();
        if (size != 1 && size % 2 == 1) {
            trades.add(trades.get(size - 1));
        }

        int merkleCount = size * 2 - 1; // 满2叉树性质
        merkles = new Merkle[merkleCount];
        int j = size - 1;
        for (int i = merkleCount; i > 0; i--) {
            if (2 * i > merkleCount) {  // 完全2叉树的线性存储，当不越界时，左子为2i，右子为2i+1；越界后为叶子节点
                merkles[i - 1] = new Merkle(trades.get(j--));
            } else {
                merkles[i - 1] = new Merkle(merkles[2 * i - 1], merkles[2 * i + 1 - 1]);
            }
        }
        root = merkles[0];
    }

    void save() throws IOException {
        File file = new File("merkle_tree/" + root.getHash().toHex());
        file.getParentFile().mkdirs();
        file.createNewFile();

        FileWriter fw = new FileWriter(file);
        for (Merkle m : merkles) {
            fw.write(m.getHash().toHex());
        }
        fw.flush();
        fw.close();
    }

}
