import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class Binary {
    byte[] bytes;
    int length;

    Binary() {
        bytes = new byte[0];
        length = 0;
    }

    Binary(byte[] b) {
        bytes = b;
        length = b.length;
    }


    static Binary parseHex(String hex) {
//        if (hex.length() % 2 != 0) hex = "0" + hex;

        ByteBuffer bb = ByteBuffer.allocate(hex.length() / 2);

        for (int i = 0; i < hex.length() / 2; i++) {
            byte b = (byte)Integer.valueOf(hex.substring(2 * i, 2 * i + 2), 16).intValue();
            bb.put(b);
        }

        Binary ans = new Binary(bb.array());
        return ans;
    }

    String toHex() {
        StringBuilder sb = new StringBuilder();
        String stmp;
        for (byte aByte : bytes) {
            stmp = Integer.toHexString(aByte & 0xFF);
            sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
        }
        return sb.toString();
    }

    public String toString() {
        return new String(bytes);
    }

    // 只保留低4字节的东西
    int toUInt() {
        return new BigInteger(1, bytes).intValue();
    }

    void append(Binary b) {
        ByteBuffer bb = ByteBuffer.allocate(this.length + b.length);
        bb.put(this.bytes);
        bb.put(b.bytes);
        this.bytes = bb.array();
        this.length = bytes.length;
    }

    void append(byte[] ba) {
        ByteBuffer bb = ByteBuffer.allocate(this.length + ba.length);
        bb.put(this.bytes);
        bb.put(ba);
        this.bytes = bb.array();
        this.length = bytes.length;
    }

    void append(int num) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        for (int i = 0; i < 4; i++) {
            byte temp = (byte) ((num >> ((3 - i) * 8)) & 0xff);
            buffer.put(temp);
        }
        this.append(buffer.array());
    }

    void append(long num) {
        append((int)num);
    }

    Binary split(int start, int end) {
        byte[] b = new byte[end - start];
        for (int i = 0; i < end - start; i++) {
            b[i] = bytes[i + start];
        }
        return new Binary(b);
    }

    Binary add (Binary b) {
        ByteBuffer bb = ByteBuffer.allocate(this.length + b.length);
        bb.put(this.bytes);
        bb.put(b.bytes);
        return new Binary(bb.array());
    }

    Binary sha256() {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(bytes);
            byte byteBuffer[] = messageDigest.digest();
            return new Binary(byteBuffer);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    Binary hash160() {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(bytes);
            byte[] mid = md.digest();
            return new Binary(mid);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    Binary secp256k1() throws IOException {
        String[] ans = Utils.runCmd("python -m secp256k1 privkey -p -k " + this.toHex());
        if (ans.length > 1) {
            return Binary.parseHex(ans[1].substring(12));
        }
        return null;
    }

    static boolean checkSig(String msg, Binary pubkey, Binary sig) throws IOException {
        String ans = Utils.runCmd("python -m secp256k1 checksig" +
                " -p " + pubkey.toHex() +
                " -m " + msg +
                " -s " + sig.toHex())[0];
        return ans.equals("True");
    }

    static Binary sign(String message, Binary privKey) throws IOException {
        return Binary.parseHex(Utils.runCmd("python -m secp256k1 sign -k " + privKey.toHex() + " -m " + message)[0]);
    }

    boolean equals(Binary b) {
        boolean ans = true;
        ans = this.length == b.length;
        for (int i = 0; i < length; i ++) {
            if (this.bytes[i] == b.bytes[i]) ans = false;
        }
        return ans;
    }


}
