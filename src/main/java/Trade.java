import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Trade {

    static class Input {
        String txid;
        long outputIndex;
        long scriptSize;
        String script;
        int SN = 0xFFFFFFFF;

        byte[] bytes() {
            scriptSize = script.getBytes().length;
            ByteBuffer bb = ByteBuffer.allocate(44 + (int) scriptSize);
            bb.put(Binary.parseHex(txid).bytes);
            Utils.memcpy((int) outputIndex, bb, 4);
            Utils.memcpy((int) scriptSize, bb, 4);
            bb.put(script.getBytes());
            Utils.memcpy(SN, bb, 4);
            return bb.array();
        }

        Input(JSONObject json) {
            txid = json.getString("txid");
            script = json.getString("script");

            SN = json.getInt("SN");
            outputIndex = json.getInt("outputIndex");
            scriptSize = json.getInt("scriptSize");
        }

        Input() {
            txid = "";
            outputIndex = 0;
            scriptSize = 0;
            script = "";
        }

        JSONObject toJSON() {
            JSONObject ans = new JSONObject();
            ans.put("txid", txid);
            ans.put("script", script);
            ans.put("scriptSize", (int) scriptSize);
            ans.put("outputIndex", (int) outputIndex);
            ans.put("SN", SN);
            return ans;
        }

        int size() {
            return 44 + (int) scriptSize;
        }
    }

    long version;
    long inputCount;
    long outputCount;
    long timeStamp;

    List<UTXO> outputs = new ArrayList<UTXO>();

    List<Input> inputs = new ArrayList<Input>();

    Trade(JSONObject json) {
        version = json.getInt("version");
        inputCount = json.getInt("inputCount");
        outputCount = json.getInt("outputCount");
        timeStamp = json.getInt("timeStamp");

        JSONArray outputJ = json.getJSONArray("outputs");
        for (int i = 0; i < outputJ.length(); i++) {
            outputs.add(new UTXO(outputJ.getJSONObject(i)));
        }


        JSONArray inputJ = json.getJSONArray("inputs");
        for (int i = 0; i < inputJ.length(); i++) {
            inputs.add(new Input(inputJ.getJSONObject(i)));
        }
    }

    JSONObject toJSON() {

        JSONObject ans = new JSONObject();
        ans.put("version", (int) version);
        ans.put("inputCount", (int) inputCount);
        ans.put("outputCount", (int) outputCount);
        ans.put("timeStamp", (int) timeStamp);

        JSONArray outputJ = new JSONArray();
        for (UTXO u : outputs) {
            outputJ.put(u.toJSON());
        }
        ans.put("outputs", outputJ);
        JSONArray inputJ = new JSONArray();
        for (Input ipt : inputs) {
            inputJ.put(ipt.toJSON());
        }
        ans.put("inputs", inputJ);

        return ans;
    }

    Trade() {
        version = 1;
        inputCount = 0;
        outputCount = 0;
        timeStamp = new Date().getTime();
        outputs = new ArrayList<UTXO>();
        inputs = new ArrayList<Input>();
    }

    byte[] bytes() {
//        int size = 16;
//        for (Input in : inputs) {
//            size += in.scriptSize + 56;
//        }
//
//        for (UTXO out : outputs) {
//            size += out.scriptSize + 12;
//        }
//
//        ByteBuffer bb = ByteBuffer.allocate(size);
//        Utils.memcpy(version, bb);
//        Utils.memcpy(inputCount, bb);
//        for (Input in : inputs) {
//            bb.put(in.bytes());
//        }
//        Utils.memcpy(outputCount, bb);
//        for (UTXO out : outputs) {
//            bb.put(out.bytes().bytes);
//        }
//        Utils.memcpy(timeStamp, bb);
//
//        return bb.array();

        Binary b = new Binary();
        b.append(version);
        b.append(inputCount);
        for (Input in : inputs) {
            b.append(in.bytes());
        }
        b.append(outputCount);
        for (UTXO out : outputs) {
            b.append(out.bytes());
        }
        b.append(timeStamp);

        return b.bytes;
    }

    int size () {
        int iSize = 0;
        for (Input i : inputs) {
            iSize += i.size();
        }

        for (UTXO u : outputs) {
            iSize += u.size();
        }
        return  iSize + 16;

    }

    static Trade.Input makeInput (String txid, long outputIndex, String script) {
        Input input = new Input();
        input.txid = txid;
        input.outputIndex = outputIndex;
        input.scriptSize = script.getBytes().length;
        input.script = script;
        return input;
    }

    void ready() {
        version = 1;
        inputCount = inputs.size();
        outputCount = outputs.size();
        timeStamp = new Date().getTime();
    }
}
